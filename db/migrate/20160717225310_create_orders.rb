class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :title
      t.text :description
      t.integer :status_id
      t.string :customer
      t.string :priority

      t.timestamps null: false
    end
    add_index :orders, :status_id
  end
end
