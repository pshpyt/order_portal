# Generated with RailsBricks
# Initial seed file to use with Devise User Model

# Temporary admin account
u = User.new(
    email: "boss@gmail.com",
    password: "admin4you",
    admin: true
)
#u.skip_confirmation!
u.save!

(1..50).each do |i|
  u = Order.new(
      title: Faker::Name.title,
      description: Faker::Hipster.sentence(3, true),
      customer: Faker::Name.name,
      priority: 'low'
  )
  u.save!

  puts "#{i} test orders created..." if (i % 5 == 0)

end
# Test user accounts
(1..50).each do |i|
begin
  u = User.new(
      email: "user#{i}@example.com",
      password: "1234"
  )
 # u.skip_confirmation!
  u.save!

  puts "#{i} test users created..." if (i % 5 == 0)
end
end
