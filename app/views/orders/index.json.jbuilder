json.array!(@orders) do |order|
  json.extract! order, :id, :title, :description, :status_id, :customer, :priority
  json.url order_url(order, format: :json)
end
